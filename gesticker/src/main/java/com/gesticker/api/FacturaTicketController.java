
package com.gesticker.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gesticker.entity.FacturaTicketEntity;
import com.gesticker.service.FacturaTicketService;

@CrossOrigin(origins="*",maxAge=3600)
@RestController
@RequestMapping("/facturaticket")
public class FacturaTicketController {

	@Autowired
	FacturaTicketService oFacturaTicketService;

	@GetMapping("/get/{id}")
	public ResponseEntity<FacturaTicketEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<FacturaTicketEntity>(oFacturaTicketService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<FacturaTicketEntity>> get() {
		return new ResponseEntity<List<FacturaTicketEntity>>(oFacturaTicketService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oFacturaTicketService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<FacturaTicketEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<FacturaTicketEntity>>(oFacturaTicketService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody FacturaTicketEntity oFacturaTicketEntity) {
		return new ResponseEntity<Boolean>(oFacturaTicketService.insert(oFacturaTicketEntity), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody FacturaTicketEntity oFacturaTicketEntity) {
		return new ResponseEntity<Boolean>(oFacturaTicketService.update(oFacturaTicketEntity), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oFacturaTicketService.delete(id), HttpStatus.OK);
	}

}
