
package com.gesticker.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gesticker.entity.MaterialEntity;
import com.gesticker.service.MaterialService;

@CrossOrigin(origins="*",maxAge=3600)
@RestController
@RequestMapping("/material")
public class MaterialController {

	@Autowired
	MaterialService oMaterialService;

	@GetMapping("/get/{id}")
	public ResponseEntity<MaterialEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<MaterialEntity>(oMaterialService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<MaterialEntity>> get() {
		return new ResponseEntity<List<MaterialEntity>>(oMaterialService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oMaterialService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<MaterialEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<MaterialEntity>>(oMaterialService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody MaterialEntity oMaterialEntity) {
		return new ResponseEntity<Boolean>(oMaterialService.insert(oMaterialEntity), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody MaterialEntity oMaterialEntity) {
		return new ResponseEntity<Boolean>(oMaterialService.update(oMaterialEntity), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oMaterialService.delete(id), HttpStatus.OK);
	}

}
