
package com.gesticker.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gesticker.entity.ServicioEntity;
import com.gesticker.service.ServicioService;

@CrossOrigin(origins="*",maxAge=3600)
@RestController
@RequestMapping("/servicio")
public class ServicioController {

	@Autowired
	ServicioService oServicioService;

	@GetMapping("/get/{id}")
	public ResponseEntity<ServicioEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<ServicioEntity>(oServicioService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<ServicioEntity>> get() {
		return new ResponseEntity<List<ServicioEntity>>(oServicioService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oServicioService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<ServicioEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<ServicioEntity>>(oServicioService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody ServicioEntity oServicioEntity) {
		return new ResponseEntity<Boolean>(oServicioService.insert(oServicioEntity), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody ServicioEntity oServicioEntity) {
		return new ResponseEntity<Boolean>(oServicioService.update(oServicioEntity), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oServicioService.delete(id), HttpStatus.OK);
	}

}
