
package com.gesticker.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gesticker.entity.UsuarioEntity;
import com.gesticker.service.UsuarioService;

@CrossOrigin(origins="*",maxAge=3600)
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioService oUsuarioService;

	@GetMapping("/get/{id}")
	public ResponseEntity<UsuarioEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<UsuarioEntity>(oUsuarioService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<UsuarioEntity>> get() {
		return new ResponseEntity<List<UsuarioEntity>>(oUsuarioService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oUsuarioService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<UsuarioEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<UsuarioEntity>>(oUsuarioService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody UsuarioEntity oUsuarioEntity) {
		return new ResponseEntity<Boolean>(oUsuarioService.insert(oUsuarioEntity), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody UsuarioEntity oUsuarioEntity) {
		return new ResponseEntity<Boolean>(oUsuarioService.update(oUsuarioEntity), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oUsuarioService.delete(id), HttpStatus.OK);
	}

}
