
package com.gesticker.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gesticker.entity.TipoUsuarioEntity;
import com.gesticker.service.TipoUsuarioService;

@CrossOrigin(origins="*",maxAge=3600)
@RestController
@RequestMapping("/tipousuario")
public class TipoUsuarioController {

	@Autowired
	TipoUsuarioService oTipoUsuarioService;

	@GetMapping("/get/{id}")
	public ResponseEntity<TipoUsuarioEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<TipoUsuarioEntity>(oTipoUsuarioService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<TipoUsuarioEntity>> get() {
		return new ResponseEntity<List<TipoUsuarioEntity>>(oTipoUsuarioService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oTipoUsuarioService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<TipoUsuarioEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<TipoUsuarioEntity>>(oTipoUsuarioService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody TipoUsuarioEntity oTipoUsuarioEntity) {
		return new ResponseEntity<Boolean>(oTipoUsuarioService.insert(oTipoUsuarioEntity), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody TipoUsuarioEntity oTipoUsuarioEntity) {
		return new ResponseEntity<Boolean>(oTipoUsuarioService.update(oTipoUsuarioEntity), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oTipoUsuarioService.delete(id), HttpStatus.OK);
	}

}
