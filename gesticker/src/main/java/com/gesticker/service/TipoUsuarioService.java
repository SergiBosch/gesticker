package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gesticker.dao.TipoUsuarioDao;
import com.gesticker.entity.TipoUsuarioEntity;

@Service
public class TipoUsuarioService {
	@Autowired
	TipoUsuarioDao oTipoUsuarioDao;

	
	public TipoUsuarioEntity get(int id) {
		return oTipoUsuarioDao.get(id);
	}

	
	public List<TipoUsuarioEntity> getall() {
		return oTipoUsuarioDao.getall();
	}

	
	public Long count() {
		return oTipoUsuarioDao.count();
	}

	
	public Page<TipoUsuarioEntity> getPage(Pageable oPageable) {
		return oTipoUsuarioDao.getPage(oPageable);
	}
	
	
	public boolean insert(TipoUsuarioEntity oTipoUsuarioEntity) {
		if(oTipoUsuarioDao.insert(oTipoUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	
	public boolean update(TipoUsuarioEntity oTipoUsuarioEntity) {
		if(oTipoUsuarioDao.update(oTipoUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	
	public boolean delete(int id) {
		return oTipoUsuarioDao.delete(id);
	}
}
