package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.gesticker.dao.LineaFacturaDao;
import com.gesticker.entity.LineasFacturaEntity;

@Service
public class LineaFacturaService {
	@Autowired
	LineaFacturaDao oLineaFacturaDao;

	public LineasFacturaEntity get(int id) {
		return oLineaFacturaDao.get(id);
	}

	public List<LineasFacturaEntity> getall() {
		return oLineaFacturaDao.getall();
	}

	public Long count() {
		return oLineaFacturaDao.count();
	}

	public Page<LineasFacturaEntity> getPage(Pageable oPageable) {
		return oLineaFacturaDao.getPage(oPageable);
	}
	
	public boolean insert(LineasFacturaEntity oLineasFacturaEntity) {
		if(oLineaFacturaDao.insert(oLineasFacturaEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(LineasFacturaEntity oLineasFacturaEntity) {
		if(oLineaFacturaDao.update(oLineasFacturaEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oLineaFacturaDao.delete(id);
	}
}
