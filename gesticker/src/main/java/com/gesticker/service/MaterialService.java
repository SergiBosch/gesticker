package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.gesticker.dao.MaterialDao;
import com.gesticker.entity.MaterialEntity;

@Service
public class MaterialService {
	@Autowired
	MaterialDao oMaterialDao;

	public MaterialEntity get(int id) {
		return oMaterialDao.get(id);
	}

	public List<MaterialEntity> getall() {
		return oMaterialDao.getall();
	}

	public Long count() {
		return oMaterialDao.count();
	}

	public Page<MaterialEntity> getPage(Pageable oPageable) {
		return oMaterialDao.getPage(oPageable);
	}
	
	public boolean insert(MaterialEntity oMaterialEntity) {
		if(oMaterialDao.insert(oMaterialEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(MaterialEntity oMaterialEntity) {
		if(oMaterialDao.update(oMaterialEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oMaterialDao.delete(id);
	}
}
