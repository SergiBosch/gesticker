package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.gesticker.dao.FacturaTicketDao;
import com.gesticker.entity.FacturaTicketEntity;

@Service
public class FacturaTicketService {
	@Autowired
	FacturaTicketDao oFacturaTicketDao;

	public FacturaTicketEntity get(int id) {
		return oFacturaTicketDao.get(id);
	}

	public List<FacturaTicketEntity> getall() {
		return oFacturaTicketDao.getall();
	}

	public Long count() {
		return oFacturaTicketDao.count();
	}

	public Page<FacturaTicketEntity> getPage(Pageable oPageable) {
		return oFacturaTicketDao.getPage(oPageable);
	}
	
	public boolean insert(FacturaTicketEntity oFacturaTicketEntity) {
		if(oFacturaTicketDao.insert(oFacturaTicketEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(FacturaTicketEntity oFacturaTicketEntity) {
		if(oFacturaTicketDao.update(oFacturaTicketEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oFacturaTicketDao.delete(id);
	}
}
