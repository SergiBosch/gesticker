package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.gesticker.dao.ServicioDao;
import com.gesticker.entity.ServicioEntity;

@Service
public class ServicioService {
	@Autowired
	ServicioDao oServicioDao;

	public ServicioEntity get(int id) {
		return oServicioDao.get(id);
	}

	public List<ServicioEntity> getall() {
		return oServicioDao.getall();
	}

	public Long count() {
		return oServicioDao.count();
	}

	public Page<ServicioEntity> getPage(Pageable oPageable) {
		return oServicioDao.getPage(oPageable);
	}
	
	public boolean insert(ServicioEntity oServicioEntity) {
		if(oServicioDao.insert(oServicioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(ServicioEntity oServicioEntity) {
		if(oServicioDao.update(oServicioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oServicioDao.delete(id);
	}
}
