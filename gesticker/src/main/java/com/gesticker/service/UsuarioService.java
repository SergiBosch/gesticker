package com.gesticker.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.gesticker.dao.UsuarioDao;
import com.gesticker.entity.UsuarioEntity;

@Service
public class UsuarioService {
	@Autowired
	UsuarioDao oUsuarioDao;

	public UsuarioEntity get(int id) {
		return oUsuarioDao.get(id);
	}

	public List<UsuarioEntity> getall() {
		return oUsuarioDao.getall();
	}

	public Long count() {
		return oUsuarioDao.count();
	}

	public Page<UsuarioEntity> getPage(Pageable oPageable) {
		return oUsuarioDao.getPage(oPageable);
	}
	
	public boolean insert(UsuarioEntity oUsuarioEntity) {
		if(oUsuarioDao.insert(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(UsuarioEntity oUsuarioEntity) {
		if(oUsuarioDao.update(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oUsuarioDao.delete(id);
	}
}
