package com.gesticker.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "usuario")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsuarioEntity implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no cale
	private int id;
	@Column(name = "dni")
	private String dni;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "apellido1")
	private String apellido1;
	@Column(name = "apellido2")
	private String apellido2;
	@Column(name = "email")
	private String email;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String password;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="tipo_usuario_id")
	private TipoUsuarioEntity tipo_usuario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "trabajador", cascade = { CascadeType.ALL })
	private List<FacturaTicketEntity> tickets_trabajador = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", cascade = { CascadeType.ALL })
	private List<FacturaTicketEntity> facturas_cliente = new ArrayList<>();
	
	public UsuarioEntity() {}

	public UsuarioEntity(String dni, String nombre, String apellido1, String apellido2, String email, String login,
			String password, TipoUsuarioEntity tipo_usuario) {
		this.dni = dni;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.email = email;
		this.login = login;
		this.password = password;
		this.tipo_usuario = tipo_usuario;
	}

	public UsuarioEntity(int id, String dni, String nombre, String apellido1, String apellido2, String email,
			String login, String password, TipoUsuarioEntity tipo_usuario) {
		this.id = id;
		this.dni = dni;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.email = email;
		this.login = login;
		this.password = password;
		this.tipo_usuario = tipo_usuario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public TipoUsuarioEntity getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(TipoUsuarioEntity tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}
	
	

}
