package com.gesticker.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "factura_ticket")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FacturaTicketEntity implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no cale
	private int id;
	@Column(name = "fecha_alta")
	private Date fecha_alta;
	@Column(name = "fecha_fin")
	private Date fecha_fin;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cliente")
	private UsuarioEntity cliente;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="trabajador")
	private UsuarioEntity trabajador;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "factura", cascade = { CascadeType.ALL })
	private List<LineasFacturaEntity> lineas_factura = new ArrayList<>();
	
	public FacturaTicketEntity() {}

	public FacturaTicketEntity(Date fecha_alta, Date fecha_fin, UsuarioEntity cliente, UsuarioEntity trabajador) {
		super();
		this.fecha_alta = fecha_alta;
		this.fecha_fin = fecha_fin;
		this.cliente = cliente;
		this.trabajador = trabajador;
	}

	public FacturaTicketEntity(int id, Date fecha_alta, Date fecha_fin, UsuarioEntity cliente,
			UsuarioEntity trabajador) {
		super();
		this.id = id;
		this.fecha_alta = fecha_alta;
		this.fecha_fin = fecha_fin;
		this.cliente = cliente;
		this.trabajador = trabajador;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public UsuarioEntity getCliente() {
		return cliente;
	}

	public void setCliente(UsuarioEntity cliente) {
		this.cliente = cliente;
	}

	public UsuarioEntity getTrabajador() {
		return trabajador;
	}

	public void setTrabajador(UsuarioEntity trabajador) {
		this.trabajador = trabajador;
	}

	public List<LineasFacturaEntity> getLineas_factura() {
		return lineas_factura;
	}

	public void setLineas_factura(List<LineasFacturaEntity> lineas_factura) {
		this.lineas_factura = lineas_factura;
	}
	
	

	
}
