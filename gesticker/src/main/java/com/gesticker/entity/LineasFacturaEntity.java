package com.gesticker.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "lineas_factura")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class LineasFacturaEntity implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no cale
	private int id;
	@Column(name = "horas_invertidas")
	private Double horas_invertidas;
	@Column(name = "cantidad_material")
	private Double cantidad_material;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="factura_id")
	private FacturaTicketEntity factura;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="material_id")
	private MaterialEntity material;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="servicio_id")
	private ServicioEntity servicio;
	
	public LineasFacturaEntity() {}

	public LineasFacturaEntity(Double horas_invertidas, Double cantidad_material, FacturaTicketEntity factura,
			MaterialEntity material, ServicioEntity servicio) {
		this.horas_invertidas = horas_invertidas;
		this.cantidad_material = cantidad_material;
		this.factura = factura;
		this.material = material;
		this.servicio = servicio;
	}

	public LineasFacturaEntity(int id, Double horas_invertidas, Double cantidad_material, FacturaTicketEntity factura,
			MaterialEntity material, ServicioEntity servicio) {
		this.id = id;
		this.horas_invertidas = horas_invertidas;
		this.cantidad_material = cantidad_material;
		this.factura = factura;
		this.material = material;
		this.servicio = servicio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getHoras_invertidas() {
		return horas_invertidas;
	}

	public void setHoras_invertidas(Double horas_invertidas) {
		this.horas_invertidas = horas_invertidas;
	}

	public Double getCantidad_material() {
		return cantidad_material;
	}

	public void setCantidad_material(Double cantidad_material) {
		this.cantidad_material = cantidad_material;
	}

	public FacturaTicketEntity getFactura() {
		return factura;
	}

	public void setFactura(FacturaTicketEntity factura) {
		this.factura = factura;
	}

	public MaterialEntity getMaterial() {
		return material;
	}

	public void setMaterial(MaterialEntity material) {
		this.material = material;
	}

	public ServicioEntity getServicio() {
		return servicio;
	}

	public void setServicio(ServicioEntity servicio) {
		this.servicio = servicio;
	}

	
	
}
