package com.gesticker.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "material")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MaterialEntity implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no cale
	private int id;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "precio")
	private Double precio;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "material", cascade = { CascadeType.ALL })
	private List<LineasFacturaEntity> lineas_factura = new ArrayList<>();
	
	public MaterialEntity() {}
	
	public MaterialEntity(String descripcion, Double precio, List<UsuarioEntity> lineas_factura) {
		this.descripcion = descripcion;
		this.precio = precio;
	}

	public MaterialEntity(int id, String descripcion, Double precio, List<UsuarioEntity> lineas_factura) {
		this.id = id;
		this.descripcion = descripcion;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	
	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public List<LineasFacturaEntity> getLineas_factura() {
		return lineas_factura;
	}

	public void setLineas_factura(List<LineasFacturaEntity> lineas_factura) {
		this.lineas_factura = lineas_factura;
	}
	
	

}
