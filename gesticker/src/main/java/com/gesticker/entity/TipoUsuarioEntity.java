package com.gesticker.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tipo_usuario")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TipoUsuarioEntity implements Serializable{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id") // si tiene el mismo nombre no cale
	private int id;
	@Column(name = "descripcion")
	private String descripcion;
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tipo_usuario", cascade = { CascadeType.ALL }, orphanRemoval = true)
	private List<UsuarioEntity> usuarios = new ArrayList<>();
	
	public TipoUsuarioEntity() {
		super();
	}

	public TipoUsuarioEntity(String descripcion) {
		super();
		this.descripcion = descripcion;
	}

	public TipoUsuarioEntity(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	
	public int getId() {
		return id;
	}

	
	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
