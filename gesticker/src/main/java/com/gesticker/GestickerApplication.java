package com.gesticker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestickerApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(GestickerApplication.class, args);
	}

}
