package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.FacturaTicketEntity;

@Repository
public class FacturaTicketDao {

	@Autowired
	protected FacturaTicketDaoJpaInterface oJpaRepository;

	@Autowired
	public FacturaTicketDao(FacturaTicketDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	public FacturaTicketEntity get(int id) {
		return (FacturaTicketEntity) oJpaRepository.getOne((long) id);
	}

	public List<FacturaTicketEntity> getall() {
		return oJpaRepository.findAll();
	}

	public long count() {
		return oJpaRepository.count();
	}

	public Page<FacturaTicketEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	public FacturaTicketEntity insert(FacturaTicketEntity oFacturaTicketEntity) {
		return (FacturaTicketEntity) oJpaRepository.save(oFacturaTicketEntity);
	}
	
	public FacturaTicketEntity update(FacturaTicketEntity oFacturaTicketEntity) {
		return (FacturaTicketEntity) oJpaRepository.save(oFacturaTicketEntity);
	}
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
