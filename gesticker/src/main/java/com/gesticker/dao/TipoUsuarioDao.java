package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.TipoUsuarioEntity;

@Repository
public class TipoUsuarioDao {

	@Autowired
	
	protected TipoUsuarioDaoJpaInterface oJpaRepository;

	@Autowired
	public TipoUsuarioDao(TipoUsuarioDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	
	public TipoUsuarioEntity get(int id) {
		return (TipoUsuarioEntity) oJpaRepository.getOne((long) id);
	}

	
	public List<TipoUsuarioEntity> getall() {
		return oJpaRepository.findAll();
	}

	
	public long count() {
		return oJpaRepository.count();
	}

	
	public Page<TipoUsuarioEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	
	public TipoUsuarioEntity insert(TipoUsuarioEntity oTipoUsuarioEntity) {
		return (TipoUsuarioEntity) oJpaRepository.save(oTipoUsuarioEntity);
	}
	
	
	public TipoUsuarioEntity update(TipoUsuarioEntity oTipoUsuarioEntity) {
		return (TipoUsuarioEntity) oJpaRepository.save(oTipoUsuarioEntity);
	}
	
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
