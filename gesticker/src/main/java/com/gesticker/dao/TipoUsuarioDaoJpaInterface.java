package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.TipoUsuarioEntity;


public interface TipoUsuarioDaoJpaInterface extends JpaRepository<TipoUsuarioEntity, Long> {

}
