package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.MaterialEntity;

@Repository
public class MaterialDao {

	@Autowired
	protected MaterialDaoJpaInterface oJpaRepository;

	@Autowired
	public MaterialDao(MaterialDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	public MaterialEntity get(int id) {
		return (MaterialEntity) oJpaRepository.getOne((long) id);
	}

	public List<MaterialEntity> getall() {
		return oJpaRepository.findAll();
	}

	public long count() {
		return oJpaRepository.count();
	}

	public Page<MaterialEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	public MaterialEntity insert(MaterialEntity oMaterialEntity) {
		return (MaterialEntity) oJpaRepository.save(oMaterialEntity);
	}
	
	public MaterialEntity update(MaterialEntity oMaterialEntity) {
		return (MaterialEntity) oJpaRepository.save(oMaterialEntity);
	}
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
