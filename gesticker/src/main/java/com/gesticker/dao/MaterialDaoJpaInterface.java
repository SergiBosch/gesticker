package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.MaterialEntity;

public interface MaterialDaoJpaInterface extends JpaRepository<MaterialEntity, Long> {

}
