package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.ServicioEntity;

@Repository
public class ServicioDao {

	@Autowired
	protected ServicioDaoJpaInterface oJpaRepository;

	@Autowired
	public ServicioDao(ServicioDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	public ServicioEntity get(int id) {
		return (ServicioEntity) oJpaRepository.getOne((long) id);
	}

	public List<ServicioEntity> getall() {
		return oJpaRepository.findAll();
	}

	public long count() {
		return oJpaRepository.count();
	}

	public Page<ServicioEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	public ServicioEntity insert(ServicioEntity oServicioEntity) {
		return (ServicioEntity) oJpaRepository.save(oServicioEntity);
	}
	
	public ServicioEntity update(ServicioEntity oServicioEntity) {
		return (ServicioEntity) oJpaRepository.save(oServicioEntity);
	}
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
