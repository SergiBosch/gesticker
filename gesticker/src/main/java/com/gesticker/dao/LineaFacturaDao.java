package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.LineasFacturaEntity;

@Repository
public class LineaFacturaDao {

	@Autowired
	protected LineaFacturaDaoJpaInterface oJpaRepository;

	@Autowired
	public LineaFacturaDao(LineaFacturaDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	public LineasFacturaEntity get(int id) {
		return (LineasFacturaEntity) oJpaRepository.getOne((long) id);
	}

	public List<LineasFacturaEntity> getall() {
		return oJpaRepository.findAll();
	}

	public long count() {
		return oJpaRepository.count();
	}

	public Page<LineasFacturaEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	public LineasFacturaEntity insert(LineasFacturaEntity oLineaFacturaEntity) {
		return (LineasFacturaEntity) oJpaRepository.save(oLineaFacturaEntity);
	}
	
	public LineasFacturaEntity update(LineasFacturaEntity oLineaFacturaEntity) {
		return (LineasFacturaEntity) oJpaRepository.save(oLineaFacturaEntity);
	}
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
