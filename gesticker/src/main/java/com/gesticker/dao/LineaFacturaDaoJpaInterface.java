package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.LineasFacturaEntity;

public interface LineaFacturaDaoJpaInterface extends JpaRepository<LineasFacturaEntity, Long> {

}
