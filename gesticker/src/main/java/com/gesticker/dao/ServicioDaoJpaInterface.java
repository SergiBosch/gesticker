package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.ServicioEntity;

public interface ServicioDaoJpaInterface extends JpaRepository<ServicioEntity, Long> {

}
