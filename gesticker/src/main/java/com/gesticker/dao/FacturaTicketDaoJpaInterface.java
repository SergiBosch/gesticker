package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.FacturaTicketEntity;

public interface FacturaTicketDaoJpaInterface extends JpaRepository<FacturaTicketEntity, Long> {

}
