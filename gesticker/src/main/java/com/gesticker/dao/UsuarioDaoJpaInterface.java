package com.gesticker.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gesticker.entity.UsuarioEntity;

public interface UsuarioDaoJpaInterface extends JpaRepository<UsuarioEntity, Long> {

}
