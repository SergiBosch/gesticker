package com.gesticker.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.gesticker.entity.UsuarioEntity;

@Repository
public class UsuarioDao {

	@Autowired
	protected UsuarioDaoJpaInterface oJpaRepository;

	@Autowired
	public UsuarioDao(UsuarioDaoJpaInterface oJpaRepository) {
		this.oJpaRepository = oJpaRepository;
	}

	public UsuarioEntity get(int id) {
		return (UsuarioEntity) oJpaRepository.getOne((long) id);
	}

	public List<UsuarioEntity> getall() {
		return oJpaRepository.findAll();
	}

	public long count() {
		return oJpaRepository.count();
	}

	public Page<UsuarioEntity> getPage(Pageable oPageable) {
		return oJpaRepository.findAll(oPageable);
	}
	
	public UsuarioEntity insert(UsuarioEntity oUsuarioEntity) {
		return (UsuarioEntity) oJpaRepository.save(oUsuarioEntity);
	}
	
	public UsuarioEntity update(UsuarioEntity oUsuarioEntity) {
		return (UsuarioEntity) oJpaRepository.save(oUsuarioEntity);
	}
	
	public boolean delete(int id) {
		if(oJpaRepository.existsById((long) id)) {
			oJpaRepository.deleteById((long) id);
		}
		return !oJpaRepository.existsById((long) id);
	}
}
